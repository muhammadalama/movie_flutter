import 'package:flutter/material.dart';

enum MainDrawerItemType { cinemas, favorite, close, search }

class MainDrawer extends StatelessWidget {
  final Function(MainDrawerItemType) onItemSelect;
  final MainDrawerItemType selectedItem;

  MainDrawer({@required this.onItemSelect, this.selectedItem});

  @override
  Widget build(BuildContext context) {
    return Drawer(
      child: ListView(
        children: <Widget>[
          DrawerHeader(
            child: Text("data"),
            decoration: BoxDecoration(
              image: DecorationImage(
                  image: AssetImage("assets/images/zootopia.png"),
                  fit: BoxFit.cover),
            ),
          ),
          ListTile(
            title: Text("Search"),
            trailing: Icon(Icons.search),
            onTap: () => _onItemSelect(context, MainDrawerItemType.search),
          ),
          Divider(),
          ListTile(
            title: Text("Movies"),
            trailing: Icon(Icons.movie),
            selected: selectedItem == MainDrawerItemType.cinemas,
            onTap: () => _onItemSelect(context, MainDrawerItemType.cinemas),
          ),
          ListTile(
            title: Text("Favorite"),
            trailing: Icon(Icons.favorite),
            selected: selectedItem == MainDrawerItemType.favorite,
            onTap: () => _onItemSelect(context, MainDrawerItemType.favorite),
          ),
          Divider(),
          ListTile(
            title: Text("Close"),
            trailing: Icon(Icons.close),
            onTap: () {
              Navigator.pop(context);
            },
          )
        ],
      ),
    );
  }

  _onItemSelect(BuildContext context, MainDrawerItemType itemType) {
    Navigator.pop(context);
    onItemSelect(itemType);
  }
}
