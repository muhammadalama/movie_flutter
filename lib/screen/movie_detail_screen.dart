import 'dart:async';

import 'package:flutter/material.dart';
import 'package:movie_flutter/model/favorite.dart';
import 'package:movie_flutter/screen/screen_navigator.dart';
import 'package:movie_flutter/service/api_client.dart';
import 'package:movie_flutter/utils/bottom_gradient.dart';
import 'package:movie_flutter/model/movie.dart';
import 'package:movie_flutter/utils/utils.dart';
import 'package:movie_flutter/utils/bubble_view.dart';
import 'package:movie_flutter/model/cast.dart';

class MovieDetailScreen extends StatefulWidget {
  final ApiClient client = new ApiClient();
  final Movie movie;
  final FavoriteProvider provider = FavoriteProvider();

  MovieDetailScreen({this.movie});

  @override
  _MovieDetailScreenState createState() => _MovieDetailScreenState();
}

class _MovieDetailScreenState extends State<MovieDetailScreen> {
  final _paddingH = 16.0;
  bool _visible = false;

  final _scaffoldKey = GlobalKey<ScaffoldState>();

  final List<Cast> _casts = List<Cast>();
  final List<Movie> _similar = List<Movie>();

  Movie _movie;
  bool isFavorite = false;

  @override
  void initState() {
    super.initState();

    _refreshDetail();
    _refreshCasts();
    _refreshSimilar();
    _refreshFavorite();

    Timer(Duration(milliseconds: 100), () => setState(() => _visible = true));
  }

  void _refreshDetail() async {
    try {
      Movie movie = await widget.client.getMovieDetail(widget.movie.id);
      if (!this.mounted) return;
      setState(() {
        _movie = movie;
      });
    } catch (e) {
      if (!this.mounted) return;
      _showError(e);
    }
  }

  void _refreshCasts() async {
    try {
      List<Cast> objects = await widget.client.getCasts(widget.movie.id);
      if (!this.mounted) return;
      setState(() {
        _casts.addAll(objects);
      });
    } catch (e) {
      if (!this.mounted) return;
      _showError(e);
    }
  }

  void _refreshSimilar() async {
    try {
      List<Movie> objects =
          await widget.client.getMovieSimilar(widget.movie.id);
      if (!this.mounted) return;
      setState(() {
        _similar.addAll(objects);
      });
    } catch (e) {
      if (!this.mounted) return;
      _showError(e);
    }
  }

  void _refreshFavorite() async {
    bool isFavorite = await widget.provider.isFavorite(widget.movie.id);
    setState(() {
      this.isFavorite = isFavorite;
    });
  }

  _showError(Exception e) {
    print(e);
    _scaffoldKey.currentState
        ?.showSnackBar(SnackBar(content: Text(e.toString())));
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        key: _scaffoldKey,
        backgroundColor: Color.fromARGB(255, 34, 33, 40),
        body: CustomScrollView(
          physics: ClampingScrollPhysics(),
          slivers: [
            SliverAppBar(
              pinned: false,
              floating: true,
              snap: false,
              title: Text(""),
              expandedHeight: 224.0,
              flexibleSpace: FlexibleSpaceBar(
                title: Text(""),
                background: _buildHeader(),
              ),
              actions: [
                IconButton(
                  icon: isFavorite
                      ? Icon(Icons.favorite)
                      : Icon(Icons.favorite_border),
                  onPressed: () {
                    _onFavoriteSelect();
                  },
                )
              ],
            ),
            SliverList(
                delegate: SliverChildListDelegate([
              _buildSynopsis(),
              _buildCast(),
              _buildAbout(),
              _buildSimilar()
            ]))
          ],
        ));
  }

  void _onFavoriteSelect() async {
    bool isFavorite = await widget.provider.isFavorite(widget.movie.id);
    if (!isFavorite) {
      Favorite favorite = Favorite.fromMovie(_movie);
      await widget.provider.insert(favorite).catchError((e) {
        print("Failed to insert favorite: ${e.toString()}");
      });

      _scaffoldKey.currentState.removeCurrentSnackBar();
      _scaffoldKey.currentState.showSnackBar(SnackBar(
        content: Text('Added to favorite'),
      ));
    } else {
      await widget.provider.delete(_movie.id).catchError((e) {
        print("Failed to delete favorite: ${e.toString()}");
      });

      _scaffoldKey.currentState.removeCurrentSnackBar();
      _scaffoldKey.currentState.showSnackBar(SnackBar(
        content: Text('Removed from favorite'),
      ));
    }

    _refreshFavorite();
  }

  Widget _buildHeader() {
    return Stack(
      fit: StackFit.expand,
      alignment: Alignment.bottomLeft,
      children: [
        Hero(
          tag: widget.movie.getImageTag(),
          child: FadeInImage.assetNetwork(
            placeholder: placeholder,
            image: widget.movie.getBackDropUrl(),
            fit: BoxFit.cover,
            fadeInDuration: Duration(milliseconds: 50),
          ),
        ),
        AnimatedOpacity(
          opacity: _visible ? 1.0 : 0.0,
          duration: Duration(milliseconds: 400),
          child: Stack(
            fit: StackFit.expand,
            children: [
              BottomGradient(),
              Container(
                padding: EdgeInsets.all(_paddingH),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.end,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Row(
                      children: [
                        BubbleView(widget.movie.getReleaseYear().toString()),
                        Divider(
                          indent: 4.0,
                        ),
                        BubbleView(widget.movie.voteAverage.toString()),
                      ],
                    ),
                    Container(
                      padding: EdgeInsets.only(top: 4.0, bottom: 8.0),
                      child: Text(
                        widget.movie.title,
                        style: TextStyle(color: Colors.white, fontSize: 20.0),
                      ),
                    ),
                    _movie?.genres == null
                        ? Container()
                        : Wrap(
                            spacing: 4.0,
                            runSpacing: 4.0,
                            direction: Axis.horizontal,
                            children: _movie.genres
                                .map<BubbleView>((value) =>
                                    BubbleView(value, color: Colors.grey))
                                .toList())
                  ],
                ),
              )
            ],
          ),
        ),
      ],
    );
  }

  Widget _buildSynopsis() {
    return DefaultTextStyle.merge(
      style: TextStyle(color: Colors.white),
      child: Padding(
        padding: EdgeInsets.all(_paddingH),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Padding(
              padding: const EdgeInsets.only(bottom: 8.0),
              child: Text("SYNOPSIS"),
            ),
            Text(
              _movie?.overview ?? widget.movie.overview ?? 'loading',
              style: TextStyle(fontSize: 12.0),
            ),
          ],
        ),
      ),
    );
  }

  Widget _buildCast() {
    return DefaultTextStyle.merge(
      style: TextStyle(color: Colors.white),
      child: Container(
        padding: EdgeInsets.all(_paddingH),
        color: Color.fromARGB(255, 44, 43, 50),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Padding(
              padding: const EdgeInsets.only(bottom: 8.0),
              child: Text("Cast"),
            ),
            _casts.isEmpty
                ? Center(child: CircularProgressIndicator())
                : Container(height: 128.0, child: _buildCastList())
          ],
        ),
      ),
    );
  }

  Widget _buildCastList() {
    return ListView.builder(
      itemCount: _casts.length,
      scrollDirection: Axis.horizontal,
      itemBuilder: (context, index) {
        return _buildCastItem(_casts[index]);
      },
    );
  }

  Widget _buildCastItem(Cast cast) {
    return GestureDetector(
      onTap: () => navigateToCastDetail(context, cast: cast),
      child: Container(
        margin: EdgeInsets.only(right: 8.0),
        child: AspectRatio(
          aspectRatio: 0.76,
          child: Stack(
            fit: StackFit.expand,
            children: [
              Hero(
                tag: cast.getTagId(),
                child: FadeInImage.assetNetwork(
                  placeholder: placeholder,
                  image: cast.getProfilePath(),
                  fit: BoxFit.cover,
                  fadeInDuration: Duration(milliseconds: 50),
                ),
              ),
              BottomGradient(),
              Container(
                padding: const EdgeInsets.all(8.0),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisAlignment: MainAxisAlignment.end,
                  children: [
                    Text(
                      cast.name,
                      style: TextStyle(
                          fontSize: 10.0, fontWeight: FontWeight.bold),
                      maxLines: 2,
                      overflow: TextOverflow.ellipsis,
                    ),
                    Row(
                      children: [
                        Icon(
                          Icons.person,
                          size: 12.0,
                          color: Colors.orange,
                        ),
                        Expanded(
                            child: Text(
                          cast.character,
                          style: TextStyle(fontSize: 9.0),
                          overflow: TextOverflow.ellipsis,
                          maxLines: 1,
                        )),
                      ],
                    )
                  ],
                ),
              )
            ],
          ),
        ),
      ),
    );
  }

  Widget _buildAbout() {
    return Container(
      padding: EdgeInsets.all(_paddingH),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Padding(
            padding: EdgeInsets.only(bottom: _paddingH),
            child: Text(
              "About",
              style: TextStyle(color: Colors.white),
            ),
          ),
          _movie == null
              ? Center(child: CircularProgressIndicator())
              : Column(
                  children: [
                    _buildAboutItem("Original Title", _movie.originalTitle),
                    _buildAboutItem("Status", _movie.status),
                    _buildAboutItem("Duration", _movie.formatRuntime()),
                    _buildAboutItem("Budget", _movie.budget.toString()),
                    _buildAboutItem("Revenue", _movie.revenue.toString()),
                  ],
                )
        ],
      ),
    );
  }

  Widget _buildAboutItem(String title, String content) {
    return Container(
      padding: EdgeInsets.only(bottom: 8.0),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Container(
            width: 120.0,
            child: Text(
              title,
              style: TextStyle(color: Colors.grey, fontSize: 11.0),
            ),
          ),
          Expanded(
              child: Text(
            content ?? "",
            style: TextStyle(color: Colors.white, fontSize: 11.0),
          ))
        ],
      ),
    );
  }

  Widget _buildSimilar() {
    return DefaultTextStyle.merge(
      style: TextStyle(color: Colors.white),
      child: Container(
        color: Color.fromARGB(255, 44, 43, 50),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Container(
              padding: EdgeInsets.all(_paddingH),
              child: Text("Similar"),
            ),
            _similar.isEmpty
                ? Center(child: CircularProgressIndicator())
                : Container(
                    height: 300.0,
                    child: GridView.count(
                      physics: ClampingScrollPhysics(),
                      crossAxisCount: 2,
                      childAspectRatio: 1.5,
                      scrollDirection: Axis.horizontal,
                      children: _similar.map((item) {
                        return GestureDetector(
                          onTap: () {
                            navigateToMovieDetail(context, movie: item);
                          },
                          child: FadeInImage.assetNetwork(
                            placeholder: placeholder,
                            image: item.getPosterUrl(),
                            fit: BoxFit.cover,
                            fadeInDuration: Duration(milliseconds: 50),
                          ),
                        );
                      }).toList(),
                    ),
                  )
          ],
        ),
      ),
    );
  }
}
