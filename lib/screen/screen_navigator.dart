import 'package:flutter/material.dart';
import 'package:movie_flutter/model/cast.dart';
import 'package:movie_flutter/model/movie.dart';
import 'package:movie_flutter/screen/cast_detail_container.dart';
import 'package:movie_flutter/screen/movie_detail_screen.dart';

navigateToMovieDetail(BuildContext context, {@required Movie movie}) {
  Navigator.of(context)
      .push(new MaterialPageRoute<void>(builder: (BuildContext context) {
    return MovieDetailScreen(movie: movie);
  }));
}

navigateToCastDetail(BuildContext context, {@required Cast cast}) {
  Navigator.of(context)
      .push(new MaterialPageRoute<void>(builder: (BuildContext context) {
    return CastDetailContainer(cast: cast);
  }));
}
