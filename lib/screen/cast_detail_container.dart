import 'package:flutter/material.dart';
import 'package:movie_flutter/item/movie_item.dart';
import 'package:movie_flutter/model/cast.dart';
import 'package:movie_flutter/model/movie.dart';
import 'package:movie_flutter/screen/screen_navigator.dart';
import 'package:movie_flutter/service/api_client.dart';
import 'package:movie_flutter/utils/styles.dart';
import 'package:movie_flutter/utils/utils.dart';

class CastDetailContainer extends StatefulWidget {
  final Cast cast;

  CastDetailContainer({this.cast});

  @override
  _CastDetailContainerState createState() => _CastDetailContainerState();
}

class _CastDetailContainerState extends State<CastDetailContainer> {
  final provider = ApiClient();
  final List<Movie> _movies = List();

  var _refreshMovieKey = GlobalKey<RefreshIndicatorState>();

  @override
  void initState() {
    super.initState();

    Future.delayed(Duration(milliseconds: 200), () {
      _refreshMovieKey.currentState.show();
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Color.fromARGB(255, 44, 43, 50),
      body: DefaultTabController(
        length: 2,
        child: NestedScrollView(
          headerSliverBuilder: (context, innerBoxIsScrolled) {
            return [
              _buildAppBar(),
            ];
          },
          body: _buildBody(context),
        ),
      ),
    );
  }

  Future _onRefreshMovie() async {
    if (widget.cast?.id == null) return null;

    var movies = await provider.getMoviesForActor(widget.cast?.id);
    if (!mounted) return null;
    setState(() {
      _movies.addAll(movies);
    });

    return null;
  }

  _buildAppBar() {
    return SliverAppBar(
      expandedHeight: 200.0,
      pinned: true,
      flexibleSpace: FlexibleSpaceBar(
        background: Stack(
          children: <Widget>[
            Container(
              decoration: BoxDecoration(
                gradient: LinearGradient(colors: [
                  const Color(0xff2b5876),
                  const Color(0xff4e4376),
                ]),
              ),
            ),
            Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Hero(
                  tag: widget.cast.getTagId(),
                  child: ClipRRect(
                    borderRadius: BorderRadius.circular(46.0),
                    child: FadeInImage.assetNetwork(
                      width: 92.0,
                      height: 92.0,
                      placeholder: placeholder,
                      image: widget.cast.getProfilePath(),
                      fit: BoxFit.cover,
                      fadeInDuration: Duration(milliseconds: 50),
                    ),
                  ),
                ),
                Container(
                  height: 8.0,
                ),
                Text(
                  widget.cast.name ?? "",
                  style: whiteBody.copyWith(fontSize: 20.0),
                )
              ],
            ),
          ],
        ),
      ),
      bottom: TabBar(
        tabs: <Widget>[
          Tab(icon: Icon(Icons.movie)),
          Tab(icon: Icon(Icons.tv)),
        ],
      ),
    );
  }

  _buildBody(BuildContext context) {
    return TabBarView(
      children: <Widget>[
        RefreshIndicator(
          key: _refreshMovieKey,
          child: MediaQuery.removePadding(
            context: context,
            removeTop: true,
            child: ListView.builder(
              itemCount: _movies.length,
              itemBuilder: (context, index) {
                return MovieItem(_movies[index], () {
                  navigateToMovieDetail(context, movie: _movies[index]);
                });
              },
            ),
          ),
          onRefresh: _onRefreshMovie,
        ),
        ListView.builder(
          itemCount: 50,
          itemBuilder: (context, index) {
            return Text(
              'Index $index',
              style: whiteBody.copyWith(fontSize: 14.0),
            );
          },
        ),
      ],
    );
  }
}
