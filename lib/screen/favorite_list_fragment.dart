import 'package:flutter/material.dart';
import 'package:movie_flutter/item/movie_item.dart';
import 'package:movie_flutter/model/favorite.dart';
import 'package:movie_flutter/model/movie.dart';
import 'package:movie_flutter/screen/screen_navigator.dart';

class FavoriteListFragment extends StatefulWidget {
  final FavoriteProvider provider = FavoriteProvider();

  @override
  _FavoriteListFragmentState createState() => _FavoriteListFragmentState();
}

class _FavoriteListFragmentState extends State<FavoriteListFragment> {
  final List<Favorite> _favorites = new List();

  @override
  void initState() {
    super.initState();

    loadData();
  }

  Future loadData() async {
    List<Favorite> data = await widget.provider.getFavorites();
    _favorites.clear();
    setState(() {
      _favorites.addAll(data);
    });

    return null;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: RefreshIndicator(
      child: _favorites.isEmpty
          ? Center(child: Text('No data'))
          : ListView.builder(
              itemCount: _favorites.length,
              itemBuilder: (context, index) {
                Favorite favorite = _favorites[index];
                Movie movie = Movie(
                    id: favorite.id,
                    title: favorite.title,
                    posterPath: favorite.posterPath,
                    backdropPath: favorite.backdropPath,
                    voteAverage: favorite.voteAverage,
                    releaseDate: favorite.releaseDate,
                    genresInfo: favorite.genresInfo);
                return MovieItem(movie, () {
                  navigateToMovieDetail(context, movie: movie);
                });
              }),
      onRefresh: () => loadData(),
    ));
  }
}
