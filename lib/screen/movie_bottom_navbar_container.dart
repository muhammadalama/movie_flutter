import 'package:flutter/material.dart';
import 'package:movie_flutter/screen/movie_list_fragment.dart';

class MovieBottomNavbarContainer extends StatefulWidget {
  @override
  _MovieBottomNavbarContainerState createState() =>
      _MovieBottomNavbarContainerState();
}

class _MovieBottomNavbarContainerState
    extends State<MovieBottomNavbarContainer> {
  final List<BottomNavigationBarItem> _menuItems = [
    BottomNavigationBarItem(label: 'Popular', icon: Icon(Icons.thumb_up)),
    BottomNavigationBarItem(label: 'Upcoming', icon: Icon(Icons.timer))
  ];

  int _currentIndexPage = 0;

  final PageController _pageController = PageController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: _buildBody(),
      bottomNavigationBar: _buildBottomNavBar(),
    );
  }

  _buildBottomNavBar() {
    return BottomNavigationBar(
      currentIndex: _currentIndexPage,
      items: _menuItems,
      onTap: (index) {
        _pageController.animateToPage(index,
            duration: Duration(milliseconds: 200), curve: Curves.ease);
      },
    );
  }

  _buildBody() {
    return PageView(
      controller: _pageController,
      children: [
        MovieListFragment(category: 'popular'),
        MovieListFragment(category: 'upcoming'),
      ],
      onPageChanged: _onPageChanged,
    );
  }

  _onPageChanged(int index) {
    setState(() {
      _currentIndexPage = index;
    });
  }

  @override
  void dispose() {
    super.dispose();
    _pageController.dispose();
  }
}
