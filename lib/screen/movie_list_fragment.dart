import 'package:flutter/material.dart';
import 'package:movie_flutter/screen/screen_navigator.dart';
import 'package:movie_flutter/service/api_client.dart';
import 'package:movie_flutter/model/movie.dart';
import 'package:movie_flutter/item/movie_item.dart';

class MovieListFragment extends StatefulWidget {
  final ApiClient client = new ApiClient();
  final String category;

  MovieListFragment({Key key, @required this.category}) : super(key: key);

  @override
  MovieListFragmentState createState() => new MovieListFragmentState();
}

class MovieListFragmentState extends State<MovieListFragment>
    with AutomaticKeepAliveClientMixin<MovieListFragment> {
  final List<Movie> _movies = new List();
  int page = 0;

  final _scaffoldKey = GlobalKey<ScaffoldState>();
  final _refreshKey = GlobalKey<RefreshIndicatorState>();

  @override
  void initState() {
    super.initState();

    Future.delayed(Duration(milliseconds: 100)).then((_) {
      _refreshKey.currentState?.show();
    });
  }

  Future _onRefresh() async {
    print('refresh');

    page = 0;
    await _onRefreshNext();

    return null;
  }

  Future _onRefreshNext() async {
    try {
      List<Movie> movies = await widget.client
          .getMovieList(category: widget.category, page: page + 1);
      if (!this.mounted) return null;
      if (page == 0) _movies.clear();
      setState(() {
        _movies.addAll(movies);
        page++;
      });
    } catch (e) {
      if (!this.mounted) return null;
      _showError(e);
    }

    return null;
  }

  _showError(Exception e) {
    print(e);
    _scaffoldKey.currentState
        ?.showSnackBar(SnackBar(content: Text(e.toString())));
  }

  @override
  Widget build(BuildContext context) {
    super.build(context);
    return Scaffold(
      key: _scaffoldKey,
      body: _buildMovieList(),
    );
  }

  Widget _buildMovieList() {
    return RefreshIndicator(
      key: _refreshKey,
      child: ListView.builder(
          controller: PrimaryScrollController.of(Scaffold.of(context).context),
          itemCount: _movies.length,
          itemBuilder: (BuildContext context, int index) {
            if (index >= _movies.length - 1) {
              _onRefreshNext();
            }

            Movie movie = _movies[index];
            return MovieItem(movie, () {
              navigateToMovieDetail(context, movie: movie);
            });
          }),
      onRefresh: () => _onRefresh(),
    );
  }

  @override
  bool get wantKeepAlive => true;
}
