import 'package:flutter/material.dart';

class BubbleView extends StatelessWidget {
  final String text;
  final MaterialColor color;

  BubbleView(this.text, {this.color = Colors.orange});

  @override
  Widget build(BuildContext context) {
    return Container(
        decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(16.0), color: color),
        child: Padding(
          padding: EdgeInsets.fromLTRB(6.0, 4.0, 6.0, 4.0),
          child:
          Text(text, style: TextStyle(fontSize: 11.0, color: Colors.white)),
        ));
  }

}
