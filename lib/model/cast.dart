import 'package:json_annotation/json_annotation.dart';
import 'package:movie_flutter/utils/utils.dart';

part 'cast.g.dart';

@JsonSerializable()
class Cast {
  final int id;
  final String name;
  final String character;
  DateTime timeStamp;

  @JsonKey(name: 'profile_path')
  final String profilePath;

  Cast({
    this.id,
    this.name,
    this.character,
    this.profilePath,
    this.timeStamp,
  });

  factory Cast.fromJson(Map<String, dynamic> json) =>
      _$CastFromJson(json)..timeStamp = DateTime.now();
  Map<String, dynamic> toJson() => _$CastToJson(this);

  String getProfilePath() {
    return profilePath != null ? getMediumPictureUrl(profilePath) : placeholder;
  }

  String getTagId() => 'cast-photo-$id' + timeStamp?.toString();
}
