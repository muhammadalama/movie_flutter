import 'package:movie_flutter/utils/utils.dart';

class Movie {
  final int id;
  final String title;
  final String originalTitle;
  final String posterPath;
  final String backdropPath;
  final String overview;
  final String releaseDate;
  final String status;
  final int budget;
  final int revenue;
  final int runtime;
  final double voteAverage;
  final List<int> genreIds = [];
  final List<String> genres = [];
  final String genresInfo;
  final DateTime timeStamp;

  Movie(
      {this.id,
      this.title,
      this.originalTitle,
      this.posterPath,
      this.backdropPath,
      this.overview,
      this.releaseDate,
      this.status,
      this.budget,
      this.revenue,
      this.runtime,
      this.voteAverage,
      this.genresInfo,
      this.timeStamp});

  factory Movie.fromJson(Map<String, dynamic> json) {
    var _genreIds = (json['genre_ids'] as List<dynamic>)
        .map<int>((value) => value.toInt())
        .toList();

    Movie movie = Movie(
        id: json['id'].toInt(),
        title: json['title'] ?? "",
        originalTitle: json['original_title'] ?? "",
        posterPath: json['poster_path'] ?? "",
        backdropPath: json['backdrop_path'] ?? "",
        overview: json['overview'] ?? "",
        releaseDate: json['release_date'] ?? "",
        voteAverage: json['vote_average'].toDouble(),
        genresInfo: getGenreString(_genreIds),
        timeStamp: DateTime.now());

    movie.genreIds.addAll(_genreIds);

    return movie;
  }

  factory Movie.fromJsonSafe(Map<String, dynamic> json) {
    int id = int.tryParse(json['id'].toString());
    if (id == null) {
      return null;
    }

    List<int> _genreIds = [];
    List<String> _genreNames = [];

    dynamic objsMap = json['genre_ids'];
    if (objsMap != null) {
      objsMap?.forEach((item) {
        int data = int.tryParse(item?.toString());
        if (data != null) _genreIds.add(data);
      });
    } else {
      objsMap = json['genres'];
      objsMap?.forEach((item) {
        int data = int.tryParse(item['id'].toString());
        if (data != null) _genreIds.add(data);

        String dataString = item['name'];
        if (dataString != null) _genreNames.add(dataString);
      });
    }

    Movie obj = Movie(
        id: id,
        title: json['title'].toString(),
        originalTitle: json['original_title'].toString(),
        posterPath: json['poster_path'].toString(),
        backdropPath: json['backdrop_path'].toString(),
        overview: json['overview'].toString(),
        releaseDate: json['release_date'].toString(),
        voteAverage: double.tryParse(json['vote_average'].toString()),
        genresInfo: getGenreString(_genreIds),
        status: json['status'].toString(),
        budget: int.tryParse(json['budget'].toString()),
        revenue: int.tryParse(json['revenue'].toString()),
        runtime: int.tryParse(json['runtime'].toString()),
        timeStamp: DateTime.now());

    obj.genreIds.addAll(_genreIds);
    obj.genres.addAll(_genreNames);

    return obj;
  }

  String getBackDropUrl() => getLargePictureUrl(backdropPath);
  String getPosterUrl() => getMediumPictureUrl(posterPath);
  String getImageTag() => "tag-movie-" + id.toString() + timeStamp?.toString();

  int getReleaseYear() {
    return releaseDate == null || releaseDate == ""
        ? 0
        : DateTime.parse(releaseDate).year;
  }

  String formatRuntime() {
    if (runtime == null) return null;

    int hours = runtime ~/ 60;
    int minutes = runtime % 60;

    return '$hours\h $minutes\m';
  }
}
