import 'package:movie_flutter/model/movie.dart';
import 'package:path/path.dart';
import 'package:sqflite/sqflite.dart';

Future<String> getDatabasePath() async {
  var databasesPath = await getDatabasesPath();
  String path = join(databasesPath, "database_keren.db");
  return path;
}

final String tableFavorite = "favorite";
final String columnId = "_id";
final String columnTitle = "title";
final String columnPosterPath = "poster_path";
final String columnBackdropPath = "backdrop_path";
final String columnReleaseDate = "release_date";
final String columnVoteAverage = "vote_average";
final String columnGenresInfo = "genres_info";

class Favorite {
  int id;
  String title;
  String posterPath;
  String backdropPath;
  String releaseDate;
  double voteAverage;
  String genresInfo;

  Favorite();

  Map<String, dynamic> toMap() {
    var map = <String, dynamic>{
      columnTitle: title,
      columnPosterPath: posterPath,
      columnBackdropPath: backdropPath,
      columnReleaseDate: releaseDate,
      columnVoteAverage: voteAverage,
      columnGenresInfo: genresInfo
    };
    if (id != null) {
      map[columnId] = id;
    }
    return map;
  }

  Favorite.fromMap(Map<String, dynamic> map) {
    id = map[columnId];
    title = map[columnTitle];
    posterPath = map[columnPosterPath];
    backdropPath = map[columnBackdropPath];
    releaseDate = map[columnReleaseDate];
    voteAverage = map[columnVoteAverage].toDouble();
    genresInfo = map[columnGenresInfo];
  }

  factory Favorite.fromMovie(Movie movie) {
    if (movie == null) return null;

    Favorite favorite = Favorite();
    favorite.id = movie?.id;
    favorite.title = movie?.title;
    favorite.posterPath = movie?.posterPath ?? "";
    favorite.backdropPath = movie?.backdropPath ?? "";
    favorite.releaseDate = movie?.releaseDate ?? "";
    favorite.voteAverage = movie?.voteAverage ?? 0;
    favorite.genresInfo = movie?.genresInfo ?? "";
    return favorite;
  }
}

class FavoriteProvider {
  Database db;

  Future _open(String path) async {
    db = await openDatabase(path, version: 1,
        onCreate: (Database db, int version) async {
      await db.execute('''
create table $tableFavorite ( 
  $columnId integer primary key, 
  $columnTitle TEXT,
  $columnPosterPath TEXT,
  $columnBackdropPath TEXT,
  $columnReleaseDate TEXT,
  $columnVoteAverage REAL,
  $columnGenresInfo TEXT)
''');
    });
  }

  Future _shouldOpen() async {
    if (db == null) {
      await _open(await getDatabasePath());
    } else if (!db.isOpen) {
      await _open(await getDatabasePath());
    }
  }

  Future<List<Favorite>> getFavorites() async {
    await _shouldOpen();
    List<Map> maps = await db.query(tableFavorite, columns: [
      columnId,
      columnTitle,
      columnPosterPath,
      columnBackdropPath,
      columnReleaseDate,
      columnVoteAverage,
      columnGenresInfo
    ]);
    return maps.map<Favorite>((item) {
      return Favorite.fromMap(item);
    }).toList();
  }

  Future<Favorite> getFavorite(int id) async {
    if (id == null) return null;

    await _shouldOpen();
    List<Map> maps = await db.query(tableFavorite,
        columns: [
          columnId,
          columnTitle,
          columnPosterPath,
          columnBackdropPath,
          columnReleaseDate,
          columnVoteAverage,
          columnGenresInfo
        ],
        where: "$columnId = ?",
        whereArgs: [id]);
    if (maps.length > 0) {
      return new Favorite.fromMap(maps.first);
    }
    return null;
  }

  Future<bool> isFavorite(int id) async {
    return await getFavorite(id) != null;
  }

  Future<Favorite> insert(Favorite favorite) async {
    if (favorite == null) throw Exception('Favorite object is null.');

    await _shouldOpen();
    favorite.id = await db.insert(tableFavorite, favorite.toMap());
    return favorite;
  }

  Future<int> delete(int id) async {
    await _shouldOpen();
    return await db
        .delete(tableFavorite, where: "$columnId = ?", whereArgs: [id]);
  }

  Future<int> update(Favorite favorite) async {
    if (favorite == null) throw Exception('Favorite object is null.');

    await _shouldOpen();
    return await db.update(tableFavorite, favorite.toMap(),
        where: "$columnId = ?", whereArgs: [favorite.id]);
  }

  Future close() async => db?.close();
}
