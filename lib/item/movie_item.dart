import 'package:flutter/material.dart';
import 'package:movie_flutter/utils/utils.dart';
import 'package:movie_flutter/model/movie.dart';

class MovieItem extends StatelessWidget {
  final Movie movie;
  final Function onTap;

  MovieItem(this.movie, this.onTap);

  @override
  Widget build(BuildContext context) {
    return _buildMovieItem();
  }

  Widget _buildMovieItem() {
    return GestureDetector(
      onTap: onTap,
      child: Card(
          clipBehavior: Clip.antiAlias,
          child: Column(
            children: [
              AspectRatio(
                aspectRatio: 2.0,
                child: Hero(
                    tag: movie.getImageTag(),
                    child: FadeInImage.assetNetwork(
                      placeholder: placeholder,
                      image: movie.getBackDropUrl(),
                      fit: BoxFit.cover,
                      fadeInDuration: Duration(milliseconds: 50),
                    )),
              ),
              Container(
                padding: EdgeInsets.all(8.0),
                child: Row(
                  children: [
                    Expanded(
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(movie.title ?? "-", style: FONT_BOLD),
                          Text(movie.genresInfo ?? "")
                        ],
                      ),
                    ),
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.end,
                      children: [
                        Row(
                          children: [
                            Text("${movie.voteAverage}"),
                            Icon(Icons.star, size: 14.0)
                          ],
                        ),
                        Row(
                          children: [
                            Text(movie.getReleaseYear().toString()),
                            Icon(Icons.calendar_today, size: 14.0)
                          ],
                        ),
                      ],
                    )
                  ],
                ),
              ),
            ],
          )),
    );
  }
}
