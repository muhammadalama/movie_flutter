import 'package:flutter/material.dart';
import 'package:movie_flutter/screen/favorite_list_fragment.dart';
import 'package:movie_flutter/screen/movie_bottom_navbar_container.dart';
import 'package:movie_flutter/screen/main_drawer.dart';

void main() => runApp(new MyApp());

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'AxuMovie',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: MainContainer(),
      routes: <String, WidgetBuilder>{},
    );
  }
}

class MainContainer extends StatefulWidget {
  @override
  _MainContainerState createState() => _MainContainerState();
}

class _MainContainerState extends State<MainContainer> {
  final Map<MainDrawerItemType, String> _navTitles = {
    MainDrawerItemType.cinemas: "Cinemas",
    MainDrawerItemType.favorite: "Favorites",
  };

  MainDrawerItemType _selectedMenuItem = MainDrawerItemType.cinemas;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: _buildAppBar(),
        drawer: MainDrawer(
            onItemSelect: _onMenuItemSelect, selectedItem: _selectedMenuItem),
        body: _buildBody());
  }

  _buildAppBar() {
    return AppBar(
      title: Text(_navTitles[_selectedMenuItem]),
    );
  }

  _buildBody() {
    switch (_selectedMenuItem) {
      case MainDrawerItemType.cinemas:
        return MovieBottomNavbarContainer();
      case MainDrawerItemType.favorite:
        return FavoriteListFragment();
      default:
        return Container();
    }
  }

  _onMenuItemSelect(MainDrawerItemType itemType) {
    if (itemType == MainDrawerItemType.search) {
      return;
    }

    setState(() {
      _selectedMenuItem = itemType;
    });
  }
}
