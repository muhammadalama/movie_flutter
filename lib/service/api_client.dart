import 'dart:convert';
import 'package:flutter/foundation.dart';
import 'package:http/http.dart' as http;
import 'package:movie_flutter/model/cast.dart';
import 'package:movie_flutter/model/movie.dart';

const String MOVIEDB_API_KEY = "28717975c9e044574a580ae4bf81d2bf";

final String dummyJson = """
{
  "page": 1,
  "total_results": 19831,
  "total_pages": 992,
  "results": [
    {
      "vote_count": 1778,
      "id": 335983,
      "video": false,
      "vote_average": 6.6,
      "title": "Venom",
      "popularity": 401.852,
      "poster_path": "/2uNW4WbgBXL25BAbXGLnLqX71Sw.jpg",
      "original_language": "en",
      "original_title": "Venom",
      "genre_ids": [
        878
      ],
      "backdrop_path": "/VuukZLgaCrho2Ar8Scl9HtV3yD.jpg",
      "adult": false,
      "overview": "When Eddie Brock acquires the powers of a symbiote, he will have to release his alter-ego \\"Venom\\" to save his life.",
      "release_date": "2018-10-03"
    },
    {
      "vote_count": 455,
      "id": 424139,
      "video": false,
      "vote_average": 6.7,
      "title": "Halloween",
      "popularity": 369.614,
      "poster_path": "/bXs0zkv2iGVViZEy78teg2ycDBm.jpg",
      "original_language": "en",
      "original_title": "Halloween",
      "genre_ids": [
        27,
        53
      ],
      "backdrop_path": "/tZ358Wk4BnOc4FjdGsiexAUvCMH.jpg",
      "adult": false,
      "overview": "Laurie Strode comes to her final confrontation with Michael Myers, the masked figure who has haunted her since she narrowly escaped his killing spree on Halloween night four decades ago.",
      "release_date": "2018-10-18"
    }
  ]
}
""";

final String dummyCasts = """
{
    "id": 433498,
    "cast": [
        {
            "cast_id": 0,
            "character": "Henri 'Papillon' Charrière",
            "credit_id": "586f2178c3a368418d002268",
            "gender": 2,
            "id": 56365,
            "name": "Steve Josh",
            "order": 0,
            "profile_path": "/ibWWSRGqgxNw9SC8E8hNv1Lvob1.jpg"
        },
        {
            "cast_id": 1,
            "character": "Louis Dega",
            "credit_id": "586f2184c3a36841890022cc",
            "gender": 2,
            "id": 17838,
            "name": "Rami Malek",
            "order": 1,
            "profile_path": "/zvBCjFmedqXRqa45jlLf6vBd9Nt.jpg"
        }
    ]
}
""";

class Page {
  final int page;
  final List<dynamic> results = new List();

  Page({this.page});

  factory Page.fromJson(Map<String, dynamic> json) {
    Page obj = Page(page: json['page']);
    obj.results.addAll(json['results']);
    return obj;
  }

  factory Page.fromJsonSafe(Map<String, dynamic> json) {
    Page obj = Page(page: int.tryParse(json['page'].toString()));
    dynamic objs = json['results'];
    objs?.forEach((item) {
      if (item != null) {
        obj.results.add(item);
      }
    });
    return obj;
  }
}

List<Movie> parseMovieList(String jsonString) {
  Map<String, dynamic> jsonMap = json.decode(jsonString);
  List<Movie> movies = Page.fromJson(jsonMap)
      .results
      .map<Movie>((e) => Movie.fromJson(e))
      .toList();
  return movies;
}

List<Movie> parseMovieListSafe(String jsonString) {
  Map<String, dynamic> jsonMap = json.decode(jsonString);
  List<Movie> objs = new List();
  Page page = Page.fromJsonSafe(jsonMap);
  page.results?.forEach((item) {
    Movie obj = Movie.fromJsonSafe(item);
    if (obj != null) {
      objs.add(obj);
    }
  });
  return objs;
}

Movie parseMovieSafe(String jsonString) {
  Map<String, dynamic> jsonMap = json.decode(jsonString);
  return Movie.fromJsonSafe(jsonMap);
}

List<Cast> parseCastList(String jsonString) {
  Map<String, dynamic> jsonMap = json.decode(jsonString);
  List<Cast> objects = new List();
  jsonMap['cast'].forEach((e) {
    try {
      Cast obj = Cast.fromJson(e);
      if (obj != null) {
        objects.add(obj);
      }
    } catch (e) {
      print("Failed parse single object: ${e.toString()}");
    }
  });
  return objects;
}

class ApiClient {
  Future<List<Movie>> getMovieList({String category, int page}) async {
    final urlString =
        'https://api.themoviedb.org/3/movie/$category?api_key=$MOVIEDB_API_KEY&language=en-US&page=$page';
    print(urlString);
    final response = await http.get(urlString);

    if (response.statusCode == 200) {
      try {
        return compute(parseMovieListSafe, response.body);
      } catch (e) {
        print(e);
        throw Exception('Failed to parse json');
      }
    } else {
      throw Exception('Failed to load data');
    }
  }

  Future<Movie> getMovieDetail(int movieId) async {
    final urlString =
        'https://api.themoviedb.org/3/movie/$movieId?api_key=$MOVIEDB_API_KEY&language=en-US';
    print(urlString);
    final response = await http.get(urlString);

    if (response.statusCode == 200) {
      try {
        return compute(parseMovieSafe, response.body);
      } catch (e) {
        print(e);
        throw Exception('Failed to parse json');
      }
    } else {
      throw Exception('Failed to load data');
    }
  }

  Future<List<Cast>> getCasts(int movieId) async {
    final urlString =
        'https://api.themoviedb.org/3/movie/$movieId/credits?api_key=$MOVIEDB_API_KEY';
    print(urlString);
    final response = await http.get(urlString);

    if (response.statusCode == 200) {
      try {
        return compute(parseCastList, response.body);
      } catch (e) {
        print(e);
        throw Exception('Failed to parse json');
      }
    } else {
      throw Exception('Failed to load data');
    }
  }

  Future<List<Movie>> getMovieSimilar(int movieId) async {
    final urlString =
        'https://api.themoviedb.org/3/movie/$movieId/recommendations?api_key=$MOVIEDB_API_KEY&language=en-US';
    print(urlString);
    final response = await http.get(urlString);

    if (response.statusCode == 200) {
      try {
        return compute(parseMovieList, response.body);
      } catch (e) {
        print(e);
        throw Exception('Failed to parse json');
      }
    } else {
      throw Exception('Failed to load data');
    }
  }

  Future<List<Movie>> getMoviesForActor(int actorId) async {
    final urlString =
        'https://api.themoviedb.org/3/discover/movie?with_cast=$actorId&api_key=$MOVIEDB_API_KEY&sort_by=popularity.desc';
    print(urlString);
    final response = await http.get(urlString);

    if (response.statusCode == 200) {
      try {
        return compute(parseMovieList, response.body);
      } catch (e) {
        print(e);
        throw Exception('Failed to parse json');
      }
    } else {
      throw Exception('Failed to load data');
    }
  }

  Future<List<Movie>> getShowsForActor(int actorId) async {
    final urlString =
        'https://api.themoviedb.org/3/person/$actorId/tv_credits?api_key=$MOVIEDB_API_KEY';
    print(urlString);
    final response = await http.get(urlString);

    if (response.statusCode == 200) {
      try {
        return compute(parseMovieList, response.body);
      } catch (e) {
        print(e);
        throw Exception('Failed to parse json');
      }
    } else {
      throw Exception('Failed to load data');
    }
  }
}
